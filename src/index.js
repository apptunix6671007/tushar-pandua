// index.js
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store/store.js';
import YourComponent from './components/YourComponent.js';


ReactDOM.render(
  <Provider store={store}>
    <YourComponent />
  </Provider>,
  document.getElementById('root')
);
