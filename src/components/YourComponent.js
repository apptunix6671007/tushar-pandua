 import {createSlice} from '@reduxjs/toolkit'
 const initialState=0;

 const counterSlice = createSlice({
  name:'counter',
  initialState,
  reducers:{
    increment(state){
     state.value++;
    },
    decrement(state){
      state.value--;
    },
    incrementByAmount(state,action){
      state.value+=action.payload;
    }
  }
  
 })
 console.log(counterSlice.action)
 export const {increment, decrement, incrementByAmount} =counterSlice.action
 export default counterSlice.reducer