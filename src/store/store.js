// store.js
import { createStore, combineReducers } from 'redux';
import modalReducer from './reducers';

const rootReducer = combineReducers({
  modalReducer,
  // other reducers if needed
});

const store = createStore(rootReducer);

export default store;
